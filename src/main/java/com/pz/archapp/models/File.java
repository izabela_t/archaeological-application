package com.pz.archapp.models;

public class File {
    private int id;
    private int id_find;
    private String path;
    private String title;
    private String description;

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public int getId_find() {
        return id_find;
    }

    public String getPath() {
        return path;
    }

    public String getTitle() {
        return title;
    }

    public File(int id, int id_find, String path, String title, String description) {
        this.id = id;
        this.description = description;
        this.id_find = id_find;
        this.path = path;
        this.title = title;
    }
}
