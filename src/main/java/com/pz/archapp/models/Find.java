package com.pz.archapp.models;

import java.time.LocalDate;
import java.util.Date;

public class Find {
    private int id;
    private int id_place_found;
    private int id_place_kept;
    private int id_discoverer;
    private String title;
    private String description;
    private Date add_date;
    private Date find_date;
    private String thumbnail;


    public int getId() {
        return id;
    }

    public Date getAdd_date() {
        return add_date;
    }

    public String getDescription() {
        return description;
    }

    public Date getFind_date() {
        return find_date;
    }

    public int getId_discoverer() {
        return id_discoverer;
    }

    public int getId_place_found() {
        return id_place_found;
    }

    public int getId_place_kept() {
        return id_place_kept;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public String getTitle() {
        return title;
    }

    public Find(int id, int id_place_found, int id_place_kept, int id_discoverer, String title, String description, Date add_date, Date find_date,  String thumbnail) {
        this.id = id;
        this.add_date = add_date;
        this.description = description;
        this.find_date = find_date;
        this.id_discoverer = id_discoverer;
        this.id_place_found = id_place_found;
        this.id_place_kept = id_place_kept;
        this.thumbnail = thumbnail;
        this.title = title;
    }
}
