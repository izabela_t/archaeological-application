package com.pz.archapp.models;

public class Localisation {
    private int id;
    private int id_continent;
    private String name;
    private String description;
    private String country;
    private String state;
    private String city;
    private String postalcode;
    private String street;
    private String number;
    private String coords;


    public int getId() {
        return id;
    }

    public String getCity() {
        return city;
    }

    public String getCoords() {
        return coords;
    }

    public String getCountry() {
        return country;
    }

    public String getDescription() {
        return description;
    }

    public int getId_continent() {
        return id_continent;
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public String getState() {
        return state;
    }

    public String getStreet() {
        return street;
    }

    public Localisation(int id, int id_continent, String name, String description, String country, String state, String city, String postalcode, String street, String number, String coords) {
        this.id = id;
        this.city = city;
        this.coords = coords;
        this.country = country;
        this.description = description;
        this.id_continent = id_continent;
        this.name = name;
        this.number = number;
        this.postalcode = postalcode;
        this.state = state;
        this.street = street;
    }
}
