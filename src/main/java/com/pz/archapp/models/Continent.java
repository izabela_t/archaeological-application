package com.pz.archapp.models;

public class Continent {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Continent(int id, String name) {
        this.id = id;
        this.name = name;
    }
}
