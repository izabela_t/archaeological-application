package com.pz.archapp.models;

public class Discoverer {
    private int id;

    private String name;
    private String surname;
    private String details;

    public int getId() {
        return id;
    }

    public String getDetails() {
        return details;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public Discoverer(int id, String name, String surname, String details) {
        this.id = id;
        this.details = details;
        this.name = name;
        this.surname = surname;
    }
}
