package com.pz.archapp.models;

public class Tag {
    private int id;
    private int id_category;
    private int id_find;

    public int getId() {
        return id;
    }

    public int getId_category() {
        return id_category;
    }

    public int getId_find() {
        return id_find;
    }

    public Tag(int id, int id_category, int id_find) {
        this.id = id;
        this.id_category = id_category;
        this.id_find = id_find;
    }
}
