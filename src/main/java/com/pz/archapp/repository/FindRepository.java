package com.pz.archapp.repository;

import com.pz.archapp.models.Find;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class FindRepository extends Repository{
    public ArrayList<Find> getFinds(){
        ArrayList data = new ArrayList<Find>();
        String SQL = "SELECT * FROM finds";

        try (Connection conn = this.database.connect();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(SQL)) {
            while (rs.next()) {
                data.add(new Find(
                        rs.getInt("id"),
                        rs.getInt("id_place_found"),
                        rs.getInt("id_place_kept"),
                        rs.getInt("id_discoverer"),
                        rs.getString("title"),
                        rs.getString("description"),
                        rs.getDate("add_date"),
                        rs.getDate("find_date"),
                        rs.getString("thumbnail")
                ));

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return data;
    }
}
