package com.pz.archapp.repository;

import com.pz.archapp.models.Category;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class CategoryRepository extends Repository{
    public ArrayList<Category> getCategories(){
        ArrayList data = new ArrayList<Category>();
        String SQL = "SELECT * FROM categories";

        try (Connection conn = this.database.connect();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(SQL)) {
            while (rs.next()) {
                data.add(new Category(
                        rs.getInt("id"),
                        rs.getString("name")
                ));

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return data;
    }
}
