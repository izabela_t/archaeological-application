package com.pz.archapp.repository;

import com.pz.archapp.models.Discoverer;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DiscovererRepository extends Repository {
    public ArrayList<Discoverer> getDiscoverers(){
        ArrayList data = new ArrayList<Discoverer>();
        String SQL = "SELECT * FROM discoverers";

        try (Connection conn = this.database.connect();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(SQL)) {
            while (rs.next()) {
                data.add(new Discoverer(
                        rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("surname"),
                        rs.getString("details")
                ));

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return data;
    }
}
