package com.pz.archapp.repository;

import com.pz.archapp.models.Continent;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ContinentRepository extends Repository{
    public ArrayList<Continent> getContinents(){
        ArrayList data = new ArrayList<Continent>();
        String SQL = "SELECT * FROM continents";

        try (Connection conn = this.database.connect();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(SQL)) {
            while (rs.next()) {
                data.add(new Continent(
                        rs.getInt("id"),
                        rs.getString("name")
                ));

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return data;
    }
}
