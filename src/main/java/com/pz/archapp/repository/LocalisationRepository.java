package com.pz.archapp.repository;

import com.pz.archapp.models.Category;
import com.pz.archapp.models.Localisation;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class LocalisationRepository extends Repository{
    public ArrayList<Localisation> getLocalisations(){
        ArrayList data = new ArrayList<Localisation>();
        String SQL = "SELECT * FROM localisations";

        try (Connection conn = this.database.connect();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(SQL)) {
            while (rs.next()) {
                data.add(new Localisation(
                        rs.getInt("id"),
                        rs.getInt("id_continent"),
                        rs.getString("name"),
                        rs.getString("description"),
                        rs.getString("country"),
                        rs.getString("state"),
                        rs.getString("city"),
                        rs.getString("postalcode"),
                        rs.getString("street"),
                        rs.getString("number"),
                        rs.getString("coords")
                ));

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return data;
    }
}
