package com.pz.archapp.repository;

import com.pz.archapp.models.File;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class FileRepository extends Repository{
    public ArrayList<File> getFiles(){
        ArrayList data = new ArrayList<File>();
        String SQL = "SELECT * FROM files";

        try (Connection conn = this.database.connect();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(SQL)) {
            while (rs.next()) {
                data.add(new File(
                        rs.getInt("id"),
                        rs.getInt("id_find"),
                        rs.getString("path"),
                        rs.getString("title"),
                        rs.getString("description")
                ));

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return data;
    }
}
