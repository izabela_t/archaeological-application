package com.pz.archapp.repository;

import com.pz.archapp.models.Tag;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class TagRepository extends Repository{
    public ArrayList<Tag> getTags(){
        ArrayList data = new ArrayList<Tag>();
        String SQL = "SELECT id,id_category,id_find FROM tags";

        try (Connection conn = this.database.connect();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(SQL)) {
            while (rs.next()) {
                data.add(new Tag(
                        rs.getInt("id"),
                        rs.getInt("id_category"),
                        rs.getInt("id_find")
                ));

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return data;
    }
}
