package com.pz.archapp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {

    private String url;
    private String user;
    private String password;

    db_config config = new db_config();

    public Database(){
        this.url= config.url;
        this.user= config.user;
        this.password= config.password;
    }

    public Connection connect() {
        java.sql.Connection conn = null;
        try {
            conn = DriverManager.getConnection(url, user, password);
            System.out.println("Connected to the PostgreSQL server successfully.");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }
}
