package com.pz.archapp;

import com.pz.archapp.models.*;
import com.pz.archapp.repository.*;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;

public class HelloApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("hello-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 320, 240);
        stage.setTitle("Hello!");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        //example get data
        CategoryRepository CategoryRepo = new CategoryRepository();
        ContinentRepository ContinentRepo = new ContinentRepository();
        DiscovererRepository DiscovererRepo = new DiscovererRepository();
        FileRepository FileRepo = new FileRepository();
        FindRepository FindRepo = new FindRepository();
        LocalisationRepository LocalisationRepo = new LocalisationRepository();
        TagRepository TagRepo = new TagRepository();

        ArrayList<Category> CategoryList = new CategoryRepository().getCategories();
        ArrayList<Continent> ContinentList = new ContinentRepository().getContinents();
        ArrayList<Discoverer> DiscovererList = new DiscovererRepository().getDiscoverers();
        ArrayList<File> FileList = new FileRepository().getFiles();
        ArrayList<Find> FindList = new FindRepository().getFinds();
        ArrayList<Localisation> LocalisationList = new LocalisationRepository().getLocalisations();
        ArrayList<Tag> TagList = new TagRepository().getTags();

        for (Category x:CategoryList){System.out.println(x.getId()+x.getName());}
        for (Continent x:ContinentList){System.out.println(x.getId()+x.getName());}
        for (Discoverer x:DiscovererList){System.out.println(x.getId()+x.getName());}
        for (File x:FileList){System.out.println(x.getId()+x.getId_find());}
        for (Find x:FindList){System.out.println(x.getId()+x.getTitle()+x.getAdd_date());}
        for (Localisation x:LocalisationList){System.out.println(x.getId()+x.getName());}
        for (Tag x:TagList){System.out.println(x.getId()+x.getId_category());}


        launch();
    }
}