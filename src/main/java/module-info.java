module com.pz.archapp {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;


    opens com.pz.archapp to javafx.fxml;
    exports com.pz.archapp;
}